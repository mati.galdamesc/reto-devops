# Respuestas

Los archivos necesarios para los distintos retos estan en el directorio reto-devops/Respuestas/

# Reto 1
   1. La imagen mas pequeña de node por ahora son las versiones slim, por lo cual se procede a instalar node:17-slim
   2. El archivo DockerFile se encuentra en el directorio reto-devops/Respuestas/Reto1/

**Respuesta**
   1. Para construir la imagen, se crea con la opción build, esto se debe ejecutar dentro del directorio reto-devops/Respuestas/Reto1
      ```
      docker build -t reto1 .
      ```
   2. Para ejecutar la imagen recien creada se hace con la opción run y se enlazan los puertos 3000 de la aplicación con el 80 de mi pc
      ```
      docker run --name docker-reto1 -p 80:3000 -d reto1
      ```
   3. Se puede acceder a la app en http://localhost:80

# Reto 2

   1. Para facilitar el proceso subí la imagen de la aplicación de node a mi DockerHub en https://hub.docker.com/r/matthy11/node-app

**Respuesta**
   1. Para construir e iniciar el servicio ejecutar  docker-commpose dentro del directo retodevops/Respuestas/Reto2, aca estaran los archivo necesarios para la ejecucíon.
      ```
      docker-compose up -d
      ```
   2. El certificado puede ser creado con un certbot en el servidor y cambiar la configuración de 80 a 443

Aqui tuve algunos incenvenientes de conexión entre el docker y mi localhost, envio igualmente la respuesta incompleta

# Reto 3

En lo que respecta a CI/CD en mi trabajo actual se implementa Jenkins.

**Respuesta**
   1. Cree un pipeline, con las configurciones correspondientes. Se encuentra en la ruta reto-devops/Respuestas/Reto3


Para los siguentes retos no es tenido la oportunidad de trabajar con alguna de esas tecnologias, Como toda la infraestructura de mi trabajo se encuentra virtualizada y no en contenedores, no he tenido la suerte de configurar kubernetes ni terraform. Con respecto al monitoreo, en mi trabajo actual ocupamos Nagios que se configura a traves de snmp.
